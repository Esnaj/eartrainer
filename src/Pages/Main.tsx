import Slider from "rc-slider";
import "rc-slider/assets/index.css";
import React from "react";
import { Interval, IntervalMode } from "../components/Interval";
import { KeyBoard } from "../components/Keys";

interface IState {
    isPlaying: boolean;
}

export default class Main extends React.Component<{}, IState> {
    static intervals: [string, number][] = [
        ["Minor second", 1],
        ["Major second", 2],
        ["Minor third", 3],
        ["Major third", 4],
        ["Perfect fourth", 5],
        ["Tritone", 6],
        ["Perfect fifth", 7],
        ["Minor sixth", 8],
        ["Major sixth", 9],
        ["Minor seventh", 10],
        ["Major seventh", 11],
        ["Octave", 12],
    ];
    keyboard: (() => void)[];
    timeBetweenPairs: number;

    statusIntervals: [string, number, boolean][];
    player: NodeJS.Timeout | undefined;
    static number: number;
    timeInterval: number;

    constructor(props: {}) {
        super(props);
        this.timeBetweenPairs = 1000;
        this.timeInterval = 500;
        this.keyboard = [];
        this.statusIntervals = [];
        for (var i = 0; i < Main.intervals.length; i++) {
            this.statusIntervals = [...this.statusIntervals, [...Main.intervals[i], false]];
        }
        this.state = {
            isPlaying: false,
        };
    }

    async mainLoop() {
        while (true) {
            await this.playerLoop();

            await this.sleep(this.timeInterval);
        }
    }

    async playerLoop() {
        while (this.state.isPlaying) {
            this.play();
            await this.sleep(this.timeBetweenPairs);
        }
        return;
    }

    componentDidMount() {

        this.mainLoop();
    }

    componentWillUnmount() {
        if (this.player) clearInterval(this.player);
    }

    updateKeyboard(key: () => void) {
        this.keyboard = [...this.keyboard, key];
    }

    playLoop() {
        Main.number = Main.number + 1;

        if (this.state.isPlaying) {
            this.play();
        }
    }

    getActiveIntervals() {
        var temp = [] as number[];
        for (var i = 0; i < this.statusIntervals.length; i++) {
            if (this.statusIntervals[i][2]) {
                temp = [...temp, this.statusIntervals[i][1]];
            }
        }
        return temp;
    }

    updateInterval(i: number, isActive: boolean) {
        this.statusIntervals[i - 1][2] = isActive;
    }

    sleep(ms: number) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }
    async playInterval() {
        const activeIntervals = this.getActiveIntervals();
        if (activeIntervals.length < 1) {
            return;
        }
        const intervalToBePlayed =
            activeIntervals[Math.floor(Math.random() * activeIntervals.length)];
        const randomIndex = Math.floor(Math.random() * (this.keyboard.length - intervalToBePlayed));
        var randomNote = randomIndex;
        var nextNote = randomNote + intervalToBePlayed;

        this.keyboard[randomNote]();
        await this.sleep(this.timeInterval);
        this.keyboard[nextNote]();
    }
    async play() {
        this.playInterval();
        await this.sleep(this.timeBetweenPairs);
    }

    handleIntervalChange(e: number) {
        this.timeBetweenPairs = e;
    }

    handleIntervalPairChange(e: number) {
        this.timeInterval = e;
    }

    render() {
        return (
            <div className="flex flex-col justify-center items-center h-screen w-screen">
                <div className="flex flex-row mb-20 space-x-10 items-center h-1/3">
                    <div id="intervals" className="flex flex-col self-start flex-wrap h-full mr-80">
                        {Main.intervals.map((e) => {
                            return (
                                <Interval
                                    name={e[0]}
                                    halfsteps={e[1]}
                                    updateIntervals={this.updateInterval.bind(this)}
                                />
                            );
                        })}
                    </div>
                    <div className="flex flex-col justify-start items-start mb-20 ">
                        <div className="w-full ">
                            <div>
                                <p>Time between pairs</p>
                                <Slider
                                    defaultValue={this.timeBetweenPairs}
                                    min={100}
                                    max={1000}
                                    onChange={this.handleIntervalChange.bind(this)}
                                />
                            </div>
                            <div>
                                <p>
                                    Time interval
                                </p>
                                <Slider
                                    defaultValue={this.timeInterval}
                                    min={100}
                                    max={1000}
                                    onChange={this.handleIntervalPairChange.bind(this)}
                                />
                            </div>
                        </div>
                        <IntervalMode name="Ascending" />
                        <IntervalMode name="Descending" />
                        <IntervalMode name="Simultanious" />
                        <button
                            className="text-3xl border border-gray-400 py-2 px-5 mt-3 self-center rounded-sm shadow-lg"
                            onClick={async () => {
                                this.setState({ isPlaying: !this.state.isPlaying });
                            }}>
                            {this.state.isPlaying ? "Pause!" : "Play!"}
                        </button>
                    </div>
                </div>
                <KeyBoard setKeyboard={this.updateKeyboard.bind(this)}></KeyBoard>
            </div>
        );
    }
}
