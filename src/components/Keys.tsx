import React from "react";
import "./Keys.css";

export function KeyBoard(props: { setKeyboard: (x: () => void) => void }) {
    return (
        <div className="flex flex-row">
            <Octave octave_num={3} setKeyboard={props.setKeyboard}/>
            <Octave octave_num={4} setKeyboard={props.setKeyboard}/>
            {/* <Octave octave_num={5} setKeyboard={props.setKeyboard}/> */}
        </div>
    );
}

function rotateToNextWhiteNTimes(n: number, list: ("white-key" | "black-key")[]) {
    function rotateOnce(list: any[]) {
        var temp = list[0];
        return [...list.slice(1, list.length), temp];
    }
    for (let i = 0; i < n; i++) {
        list = rotateOnce(list);
        if (list[0] !== "white-key") {
            list = rotateOnce(list);
        }
    }
    return list;
}

export function Octave(props: {
    octave_num: number;
    starting_white_key_n?: number;
    setKeyboard: (x: () => void) => void;
}) {
    const order: ("white-key" | "black-key")[] = [
        "white-key",
        "black-key",
        "white-key",
        "black-key",
        "white-key",
        "white-key",
        "black-key",
        "white-key",
        "black-key",
        "white-key",
        "black-key",
        "white-key",
    ];
    // const current_order = rotateToNextWhiteNTimes(props.starting_white_key_n ?? 0, order);
    const note_order = ["c", "cs", "d", "ds", "e", "f", "fs", "g", "gs", "a", "as", "b"];
    return (
        <>
            {order.map((e, i) => {
                return (
                    <Key
                        note_name={(i < 9 ? props.octave_num : props.octave_num + 1) as unknown as string + "-" + note_order[i]}
                        keyType={e}
                        setKeyboard={props.setKeyboard}
                    />
                );
            })}
        </>
    );
}

export interface IProps {
    note_name: string;
    keyType: "white-key" | "black-key";
    setKeyboard: (x: () => void) => void;
}
interface IState {
    isActive: boolean;
}

class Key extends React.Component<IProps, IState> {
    classNames: string[];
    classNamesActive: string[];

    playKey() {
        this.setState({isActive:true})
        const url = './notes/mp3-Notes/' + this.props.note_name + ".mp3"
        // const url = './notes/mp3 notes/' + "woof" + ".mp3"
        console.log(this.props.note_name)
        this.play(url).then(
            () => this.setState({isActive: false}),
            () => {console.log("error while playing file!"); this.setState({isActive: false})}
        )
    }

    async play(url : string) {
        return new Promise(function(resolve, reject) {
            var audio = new Audio();
            audio.preload = "auto"
            audio.autoplay = true
            audio.onerror = reject
            audio.onended = resolve
            audio.src = url
        })
    }

    componentDidMount() {
        this.props.setKeyboard(this.playKey.bind(this))
    }
    constructor(props: IProps) {
        super(props);

        this.classNames = [props.keyType, props.keyType === "black-key" ? "bg-black" : "bg-white"];
        this.classNamesActive = [props.keyType, "bg-pink-200"];

        this.state = {
            isActive: false,
        };
    }

    render() {
        return (
            <div
                id={this.props.note_name}
                className={(this.state.isActive ? this.classNamesActive : this.classNames).join(
                    " ",
                )}
                onMouseOut={() => this.setState({ isActive: false })}
                onMouseDown={() => {
                    this.setState({
                        isActive: true,
                    });
                    return false;
                }}
                onMouseUp={() => this.setState({ isActive: false })}
                onClick={() => {
                    this.playKey();
                }}
                />
                );
    }
}
