import { useState } from "react";

export function Interval(props: {
    name: string;
    halfsteps: number;
    updateIntervals: (i: number, isActive: boolean) => void;
}) {
    var [isActive, setIsActive] = useState(false);
    const classNamesDefault = ["border border-gray-300 p-3 mb-2 ml-2"];
    const classNamesActive = ["bg-blue-300", ...classNamesDefault].join(" ");
    const classNamesInactive = ["bg-gray-200", ...classNamesDefault].join(" ");
    return (
        <button
            key={props.halfsteps}
            className={isActive ? classNamesActive : classNamesInactive}
            onClick={() => {
                props.updateIntervals(props.halfsteps, !isActive);
                setIsActive(!isActive);
            }}>
            {props.name}
        </button>
    );
}

export function IntervalMode(props: { name: string }) {
    return (
        <div className="flex flex-row items-center w-full">
            <label className="mr-4 text-3xl">{props.name}: </label>
            <input className="ml-auto form-checkbox h-6 w-6 text-pink-600" type="checkbox" />
        </div>
    );
}


