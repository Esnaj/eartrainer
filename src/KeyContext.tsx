import { createContext, ReactNode, Ref, useContext, useState } from "react";

export type availableLangs= "NL" | "EN"
const KeyBoardContext = createContext([] as (() => void)[])
const KeyBoardUpdateContext = createContext((e : () => {}) => {})




export function useKeyBoard() {
    return useContext(KeyBoardContext)
}

export function useKeyBoardUpdate() {
    return useContext(KeyBoardUpdateContext)
}
export function keyBoardHandler() {

}

export function KeyBoardProvider(props :{children: ReactNode}) {
    const initial_state: Ref<HTMLDivElement>[] = []
    const [keyBoard, setKeyBoard] = useState([] as (() => void)[])
    
    return (
        <KeyBoardContext.Provider value={keyBoard}>
            <KeyBoardUpdateContext.Provider value={(e : () => void) => setKeyBoard([...keyBoard,  e])}>
                {props.children}
            </KeyBoardUpdateContext.Provider>
        </KeyBoardContext.Provider>

    )
}
